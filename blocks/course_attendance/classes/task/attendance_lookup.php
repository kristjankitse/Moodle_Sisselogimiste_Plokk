<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A scheduled task for course_attendance looking up attendances and saving data on absentees.
 *
 * @package    course_attendance
 * @copyright  student project
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
  namespace block_course_attendance\task;

  class attendance_lookup extends \core\task\scheduled_task {
    /**
     * Get the name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        //returns the name of the scheduled task
        return get_string('taskname', 'block_course_attendance');
    }

    /**
     * Run task.
     */
    public function execute() {
        //get courses (currently all)
        //TODO: filter courses by where the functionality is enabled
        global $DB;
        $courses = $DB->get_records_sql("SELECT id FROM `mdl_course`");

        //now for each course in selected run the function:
        //get last access and if student has not accessed the course for 7 days then add record (counter +1) to database + (send mail)
        foreach ($courses as $course) {
            //get the records for that course
            $studentrecords = $DB->get_records_sql("SELECT u.id, la.courseid, DATEDIFF(NOW(), FROM_UNIXTIME(la.timeaccess)) AS 'daysfromlastcourseaccess' FROM `mdl_role_assignments` ra, `mdl_user` u, `mdl_course` c, `mdl_context` cxt, `mdl_user_lastaccess` la, `mdl_role` r WHERE ra.userid = u.id AND ra.contextid = cxt.id AND cxt.contextlevel =50 AND cxt.instanceid = c.id AND la.userid = u.id AND r.id = ra.roleid AND (roleid = 5) AND c.id=$course->id");

            //another cycle for going through students)
            foreach($studentrecords as $student){
                //if 7 days have passed since his last login (daysfromlastcourseaccess%7==0 then update counter)
                if(($student->daysfromlastcourseaccess % 7) == 0) {
                    //here 7 days have passed, so update the database:
                    //if no records then insert counter 1, else insert counter + 1 
                    $record = new \stdClass();
                    $studentid = $student->id;
                    $course = $student->courseid;
                    $current = $DB->get_records('block_course_attendance',  array('studentid'=>$studentid, 'courseid'=>$course));
                    //if no records in database:
                   
                    $record->studentid = $studentid;
                    $record->courseid = $course;
                    if (count($current) == 0) {
                        //insert
                        $record->absentcount = 1;
                        $lastinsertid = $DB->insert_record('block_course_attendance', $record, false);
                    }
                    else {
                        //update counter +1
                        $first = array_shift($current);
                        $record->id = $first->id;
                        $abscount = $first->absentcount + 1;
                        $record->absentcount = $abscount;
                        $lastupdateid = $DB->update_record('block_course_attendance', $record, false);
                    }
                    unset($record);
                    //run email sending logic here:


                }
            }
        }
       
    }
  }