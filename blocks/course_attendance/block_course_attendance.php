<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form for editing HTML block instances.
 *
 * @package   block_course_attendance
 * @copyright student project
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_course_attendance extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_course_attendance');
    }

    public function get_content() {
        global $DB, $OUTPUT, $CFG, $USER;

        if ($this->content !== null) {
            return $this->content;
        }


        $courses = $DB->get_records_sql("SELECT CONCAT(cx.id, u.id), c.fullname as fullname, u.id as uid FROM mdl_course c LEFT OUTER JOIN mdl_context cx ON c.id = cx.instanceid 
LEFT OUTER JOIN mdl_role_assignments ra ON cx.id = ra.contextid AND ra.roleid = '3' LEFT OUTER JOIN mdl_user u ON ra.userid = u.id WHERE cx.contextlevel = '50'");

        //This array contains all the courses that have the mail functinoality turned on
        $course_array = array();
        $x = 0;
        foreach ($courses as $course) {

            if ($USER->id === $course->uid) {
                if ($this->config->{ratingtime . $x} === '1') {
                    array_push($course_array, $course->fullname);
                }
            }
            $x++;
        }


        $this->content         =  new stdClass;
        $this->content->text   = '';
        $this->content->footer = '';

        $c = array();
        $this->content->text .= '<div id="wrapper">';

        $this->content->text .= '<form action="" method="post"><select name="courses">';
        foreach ($courses as $course) {
            if ($USER->id === $course->uid or $USER->id === '2') {
                if ($course->fullname != "Moodle test site for project Ajajoon") {
                    $this->content->text .= '<option value=' . $course->fullname . '>' . $course->fullname . '</option>\n';
                    array_push($c, $course->fullname);
                }
            }
        }
        $this->content->text .= '</select></form>';

        //$var = $_POST['courses'];


        $this->content->text .= '</div>';


        $this->content->text .= "<ul class='list'>\n";
        /*$users = $DB->get_records_sql("SELECT lastname, firstname, id, (UNIX_TIMESTAMP() - lastaccess) as t FROM `mdl_user` WHERE UNIX_TIMESTAMP() - lastaccess > 0 ORDER by t DESC");

        foreach ($users as $user) {
            if ($user->firstname != "Guest user") {

                $this->content->text .= '<li class="listentry">';

                $this->content->text .= '<div id="wrapper">';

                $this->content->text .= '<div id="user">';

                $this->content->text .= $OUTPUT->user_picture($user, array('size' => 16, 'alttext' => false, 'link' => false)) . $user->firstname . " " . $user->lastname ;

                $this->content->text .= '</div>';

                if ($user->t < 2000) {
                    $this->content->text .= '<div id="circle1"></div>';
                }
                elseif ($user->t < 7000) {
                    $this->content->text .= '<div id="circle2"></div>';
                }
                else {
                    $this->content->text .= '<div id="circle3"></div>';
                }


                $this->content->text .= '</div>';

                $this->content->text .= "</li>\n";
            }

            $this->content->text .= '2';
            $this->content->text .= (string)$var;
        }*/
        $users = $DB->get_records_sql("SELECT u.id, u.lastname as lastname, u.firstname as firstname, b.absentcount as counts, c.fullname as cname FROM `mdl_block_course_attendance` as b JOIN `mdl_user` as u JOIN `mdl_course` as c
WHERE b.studentid = u.id and b.courseid = c.id ");

        foreach ($users as $user) {
            if ($user->firstname != "Guest user" and in_array($user->cname, $c)) {

                $this->content->text .= '<li class="listentry">';

                $this->content->text .= '<div id="wrapper">';

                $this->content->text .= '<div id="user">';

                $this->content->text .= $OUTPUT->user_picture($user, array('size' => 16, 'alttext' => false, 'link' => false)) . $user->firstname . " " . $user->lastname ;

                $this->content->text .= '</div>';

                if ($user->counts === '1') {
                    $this->content->text .= '<div id="circle1"></div>';
                }
                elseif ($user->counts === '2') {
                    $this->content->text .= '<div id="circle2"></div>';
                }
                else {
                    $this->content->text .= '<div id="circle3"></div>';
                }


                $this->content->text .= '</div>';

                $this->content->text .= "</li>\n";
            }

        }


        $this->content->text .= '</ul><div class="clearer"><!-- --></div>';

        /* for the custom messages to be used for mail then use
        $this->message1 = isset($this->config->text1) ? $this->config->text1 :  get_string('defaultmessage1', 'block_course_attendance');
        $this->message2 = isset($this->config->text2) ? $this->config->text2 :  get_string('defaultmessage2', 'block_course_attendance');
        $this->message3 = isset($this->config->text3) ? $this->config->text3 :  get_string('defaultmessage3', 'block_course_attendance');
        */


        return $this->content;
    }


}
