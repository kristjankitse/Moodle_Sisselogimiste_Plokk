<?php

class block_course_attendance_edit_form extends block_edit_form {

    protected function specific_definition($mform) {

        global $DB, $USER;

        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));



        $mform->addElement('html', '<h4 style="color:black;">Here you can configure the messages being sent to the students who are falling behind. Before the text area is the current message</h4>');


        if($this->block->config->text1==='') {
            $mform->addElement('text', 'config_text1', get_string('message1', 'block_course_attendance', get_string('defaultmessage1', 'block_course_attendance')));
            $mform->setType('config_text1', PARAM_RAW);
        }
        else{
            $mform->addElement('text', 'config_text1', get_string('message1', 'block_course_attendance', $this->block->config->text1));
            $mform->setType('config_text1', PARAM_TEXT);
        }

        if($this->block->config->text2==='') {
            $mform->addElement('text', 'config_text2', get_string('message2', 'block_course_attendance', get_string('defaultmessage2', 'block_course_attendance')));
            $mform->setType('config_text2', PARAM_RAW);
        }
        else{
            $mform->addElement('text', 'config_text2', get_string('message2', 'block_course_attendance', $this->block->config->text2));
            $mform->setType('config_text2', PARAM_TEXT);
        }

        if($this->block->config->text1==='') {
            $mform->addElement('text', 'config_text3', get_string('message3', 'block_course_attendance', get_string('defaultmessage3', 'block_course_attendance')));
            $mform->setType('config_text3', PARAM_RAW);
        }
        else{
            $mform->addElement('text', 'config_text3', get_string('message3', 'block_course_attendance', $this->block->config->text3));
            $mform->setType('config_text3', PARAM_TEXT);
        }


        $courses = $DB->get_records_sql("SELECT c.fullname as fullname, u.id as uid FROM mdl_course c LEFT OUTER JOIN mdl_context cx ON c.id = cx.instanceid LEFT OUTER JOIN mdl_role_assignments ra ON cx.id = ra.contextid AND ra.roleid = '3' LEFT OUTER JOIN mdl_user u ON ra.userid = u.id WHERE cx.contextlevel = '50'");

        $mform->addElement('html', '<h4 style="color:black;">Choose the subjects for which you want the mail functionality to be turned on</h4>');

        $x = 0;
        foreach ($courses as $course) {
            if($USER->id===$course->uid) {
                $mform->addElement('advcheckbox', 'config_ratingtime' . $x, $course->fullname);

            }
            $x++;

        }

    }
}