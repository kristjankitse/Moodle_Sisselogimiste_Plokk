<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



$string['course_attendance:addinstance'] = 'Add a new Course attendance block';
$string['course_attendance:myaddinstance'] = 'Add a new Course attendance block to the My Moodle page';
$string['pluginname'] = 'Course attendance';
$string['templatemessage'] = 'The message being sent to students who might be leaving behind is: "Dear student please get yourself together!". If you want to edit the message type it in the text box';
$string['textboxinstruction']= 'Enter message here';
$string['taskname'] = 'Find students who have been absent for a week and notify them.';
$string['defaultmessage1'] = 'Dear student please get yourself together!';
$string['defaultmessage2'] = 'You have not visited the course in a long time, please get on track';
$string['defaultmessage3'] = 'You are starting to fall behind in school!';
$string['message1'] = 'Message 1: {$a}';
$string['message2'] = 'Message 2: {$a}';
$string['message3'] = 'Message 3: {$a}';